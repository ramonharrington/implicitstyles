﻿using System;

using Xamarin.Forms;

namespace ImplicitGlobalButtonStyles
{
	public class App : Application
	{
		public App()
		{
			this.Resources = new ResourceDictionary();
			this.Resources.Add(
				new Style(typeof(Button)) {
					Setters = {
						new Setter{ Property = Button.BackgroundColorProperty, Value = Color.Green }
					},
					Triggers = {
						new Trigger(typeof(Button)) {
							Property = Button.IsEnabledProperty,
							Value = false,
							Setters = {
								new Setter{ Property = Button.BackgroundColorProperty, Value = Color.Red }
							}
						}
					}
				}
			);

			var button1 = new Button{Text = "Button1", IsEnabled=false};
			var button2 = new Button{Text="Enable Button1", Command = new Command(()=> {button1.IsEnabled = true;})};

			MainPage = new ContentPage {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Children = {
						button1,
						button2
					}
				}
			};
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

